<?php
ini_set('display_errors',1);
error_reporting(E_ALL);
?>
<!DOCTYPE html>
<html lang="en"
<html lang="en">
<head>

<!--
"Time-stamp: <Thu, 07-07-16, 13:45:57 Eastern Daylight Time>"
//-->
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while ompleting various projects.">
	<meta name="author" content="Christian Ray">
	<link rel="icon" href="favicon.ico">

	<title>Simple Employee Class</title>

<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

<!-- Optional theme -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

<!-- Note: following file is for form validation. -->
<link rel="stylesheet" href="css/formValidation.min.css"/>

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

	<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
		<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
	<![endif]-->

</head>

<body>
<?php 
require_once("person.php");
require_once("employee.php");

$employeeFname = $_POST['fname'];
$employeeLname = $_POST['lname'];
$employeeAge = $_POST['age'];
$employeeSSN = $_POST['ssn'];
$employeeGender = $_POST['gender'];

$employee1 = new Employee();
$employee2 = new Employee($employeeFname, $employeeLname, $employeeAge, $employeeSSN, $employeeGender);
?>
	<h2> Simple Employee Class</h2>
	
	<div class="table-responsive">
	<table id="myTable" class="table table-striped table-condensed" >
	<thead>
		<tr>
		<th>FName</th>
		<th>LName</th>
		<th>Age</th>
		<th>SSN</th>
		<th>Gender</th>
		</tr>
	</thead>
	<tr>
	<td><?php echo $employee1->GetFname(); ?></td>
	<td><?php echo $employee1->GetLname(); ?></td>
	<td><?php echo $employee1->GetAge(); ?></td>
	<td><?php echo $employee1->GetSSN(); ?></td>
	<td><?php echo $employee1->GetGender(); ?></td>
	</tr>
	
	<tr>
	<td><?php echo $employee2->GetFname(); ?></td>
	<td><?php echo $employee2->GetLname(); ?></td>
	<td><?php echo $employee2->GetAge(); ?></td>
	<td><?php echo $employee2->GetSSN(); ?></td>
	<td><?php echo $employee2->GetGender(); ?></td>
	</tr>
	</table>

<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcbn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

	<script src="js/ie10-viewport-bug-workaround.js"></script>
	
	<script>
	$(document).ready(function(){
			$('#myTable').DataTable({
				responsive: true
			});
	});
	</script>

	</body>
</html>

