<?php
class Employee extends Person
{
	private $ssn;
	private $gender;
	
	public function __contruct($fn = "Peter", $ln = "Paulsen", $ag = 33, $s = '123456789', $g = 'male')
	{
		$this->ssn = $s;
		$this->gender = $g;
		
		parent::__construct($fn, $ln, $ag);
		
		echo("Creating <strong>" . person::GetFname() . "" . person::GetLname() . " is " . person::GetAge() . " with ssn: " . $this->ssn . " and is " . $this->gender . "</strong> employee object from parameterized constructor (accepts five arguments): <br />");
	}
	
	function __destruct()
	{
		parent::__destruct();
		echo("Destroying <strong>" . person::GetFname() . "" . person::GetLname() . " is " . person::GetAge() . " with ssn: " . $this->ssn . " and is " . $this->gender . "</strong> employee object. <br />");
	}
	
	public function SetSSN($s = "11111111111")
	{
		$this->ssn = $s;
	}
	
	public funtion SetGender($g = 'f')
	{
		$this-gender = $g;
	}
	public function GetSSN()
	{
		$return $this->ssn;
	}
	
	public funtion GetGender()
	{
		$return $this->gender;
	}
}
?>