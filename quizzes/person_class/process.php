<?php

ini_set('display_errors',1);
error_reporting(E_ALL);

?>
<!DOCTYPE html>
<html lang="en">
	<head>
		
<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while ompleting various projects.">
	<meta name="author" content="Christian Ray">
	<link rel="icon" href="favicon.ico">
	
	<title>Simple Person Class</title>
	
<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.ss">

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>

</head>

<body>

	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" target="_self">Home</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="../index.php">LIS4381</a></li>
					<li><a href="../a1/index.php">A1</a></li>
					<li><a href="../a2/index.php">A2</a></li>
					<li><a href="../a3/index.php">A3</a></li>
					<li><a href="../a4/index.php">A4</a></li>
					<li><a href="../a5/index.php">A5</a></li>
					<li><a href="../p1/index.php">P1</a></li>
					<li><a href="../p2/index.php">P2</a></li>
					<li><a href="../test/index.php">Test</a></li>					
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</nav>
	
<?php

require_once("person.php");

$personFname = $_POST['fname'];
$personLname = $_POST['lname'];
$personAge = $_POST['age'];

$person1 = new Person();
$person2 = new Person($personFname, $personLname, $personAge);
?>

	<h2> Simple Person Class</h2>
	
<div class="table-responsive">
<table id="myTable" class="table table-striped table-condensed" >

<thead>

	<tr>
	<th>FName</th>
	<th>LName</th>
	<th>Age</th>
	</tr>
</thead>
<tr>
<td><?php echo $person1->GetFname(); ?></td>
<td><?php echo $person1->GetLname(); ?></td>
<td><?php echo $person1->GetAge(); ?></td>
</tr>

<tr>
<td><?php echo $person2->GetFname(); ?></td>
<td><?php echo $person2->GetLname(); ?></td>
<td><?php echo $person2->GetAge(); ?></td>
</tr>
</table>
</div>
	
	</div>
	</div>
	
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>
<script type="text/javascript" src="https://maxcbn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

<script src="js/ie10-viewport-bug-workaround.js"></script>

<script>
	$(document).ready(function(){
		$('#myTable').DataTable({
			responsive: true
		});
	});
	</script>
	</body>
</html>