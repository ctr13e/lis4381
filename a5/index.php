<!DOCTYPE html>
<html lang="en">
<head>
<?php
require_once "global/connection.php";

$query = "SELECT * FROM petstore ORDER BY pst_id";

$statement = $db->prepare($query);
$statement->execute();
?>

<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="My online portfolio that illustrates skills acquired while ompleting various projects.">
	<meta name="author" content="Christian Ray">
	<link rel="icon" href="favicon.ico">

	<title>LIS4381 - Assignment 5</title>


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">


<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">


<link rel="stylesheet" href="css/formValidation.min.css"/>


<link href="css/starter-template.css" rel="stylesheet">

	

</head>
<body>

			<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">			
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="#" target="_self">Home</a>
			</div>

			<div id="navbar" class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="active"><a href="../index.php">LIS4381</a></li>
					<li><a href="../a1/index.php">A1</a></li>
					<li><a href="../a2/index.php">A2</a></li>
					<li><a href="../a3/index.php">A3</a></li>
					<li><a href="../a4/index.php">A4</a></li>
					<li><a href="../a5/index.php">A5</a></li>
					<li><a href="../p1/index.php">P1</a></li>
					<li><a href="../p2/index.php">P2</a></li>
					<li><a href="../test/index.php">Test</a></li>					
				</ul>
			
		</div>
	</nav>

<?php
date_default_timezone_set('America/New_York');
$today = date("m/d/y g:ia");
echo $today;
 ?>
	


	<div class="container-fluid">
		<div class="starter-template">
			
					
					<div class="page-header">
						<h1>Assignment 5</h1>
							<h3>Basic client-side validation</h3>
								<p class="lead">Server-side validation, and prepared statements (helps prevent SQL injection). Displays user-entered data, and permits users to add data. (See P2 to update and delete pet store data.</p>
								(Table:petstore)	
					</div>

					<h2>Pet Stores</h2>
<a href="add_petstore.php">Add Pet Store</a>


<div class="table-responsive">
<table id="myTable" class="table table-striped table-condensed" >
<thead>
<tr>
<th>Name</th>
<th>Street</th>
<th>City</th>
<th>State</th>
<th>Zip</th>
<th>Phone</th>
<th>Email</th>
<th>URL</th>
<th>YTD Sales</th>
<th>Notes</th>
<th>&nbsp;</th>
<th>&nbsp;</th>
</thead>
</table>


<tr>
<td><?php echo htmlspecialchars($result['pst_name']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_street']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_city']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_state']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_zip']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_phone']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_email']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_url']); ?></td>	
<td><?php echo htmlspecialchars($result['pst_ytd_sales']); ?></td>					

<td><?php echo htmlspecialchars($result['pst_notes']); ?></td>

<td>
	<form
	onsubmit="return confirm('Do you really want to delete record?');"
	action="delete_petstore.php"
	method="post"
	id="delete_petstore">
	
	<input type="hidden" name="pst_id" value="<?php echo $result['pst_id']; ?>" />
	<input type="submit" value="Delete" />
</form>
</td>


<form action="edit_petstore.php" method="post" id="edit_petstore">

		<input type="hidden" name-"pst_id" value="<?php echo $result['pst_id']; ?>" />
		<input type="submit" value="Edit" />
		</form>

</tr>
					</div>
			</div>

			&copy; 
<?php include_once "global/footer.php";?>


			
		</div> 
 </div> 

	
	
<script type="text/javascript" src="https://code.jquery.com/jquery-2.1.4.min.js"></script>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>

  
<script type="text/javascript" src="js/formValidation/formValidation.min.js"></script> 

<script type="text/javascript" src"//cdn.datatables.net/1.10.9/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src"//cdn.datatables.net/responsive/1.0.7/js/dataTables.responsive.min.js"></script>

<script type="text/javascript" src="js/formValidation/bootstrap.min.js"></script>

<script src="js/ie10-viewport-bug-workaround.js"></script>

<script type="text/javascript">
 //See Regular Expressions: http://www.qcitr.com/usefullinks.htm#lesson7
$(document).ready(function() {

	$(document).ready(function(){
		$('#myTable').DataTable({
			"lengthMenu":[[10,25,50,-1],[10,25,50,"All"]],
			"columns";
			[
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			null,
			{ "orderable": false },
			{ "orderable": false }
			]
		});
	});
</script> 
</body>
</html>
