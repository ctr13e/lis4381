# LIS 4381

## Christian Ray

### Assignment 2 Requirements:

1. Design concert ticket calculator application
2. Design petstore database in MySQL workbench
3. Add everything to class bitbucket repository



[Petstore Database](docs/A3b.mwb "Petstore Database")

[Petstore SQL](docs/a3.sql "Petstore SQL")

![Screenshot2](img/2.png)

![Screenshot 1](img/A3b.png)