

# LIS 4381

## Christian Ray

### LIS4381 Requirements:

Course Work Links:

1. [A1 README.md](a1/A1-README.md "My A1 README.md file")

		-Install Git

		-Create repository for assignments

		-Define git commands

		-Install AMPPS, JDK, and Android Studio

		-Build Hello World.java, run through emulator

		-Push to bitbucket repository


2. [A2 README.md](a2/A2-README.md "My A2 README.md file")

		-Develop Bruchetta Application

		-Built two activities with unique widgets and buttons

		-Changed background for each activity

		-Push to bitbucket repository
		

3. [A3 README.md](a3/A3-README.md "My A3 README.md file")

		-Design concert ticket calculator application
		
		-Design petstore database in MySQL workbench
		
		-Add everything to class bitbucket repository


3. [P1 README.md](p1/P1-README.md "My P1 README.md file")

		-Design business card application
		
		-Add everything to class bitbucket repository

4. [A4 README.md](a4/A4-README.md "My A4 README.md file")

		-Design business card application
		
		-Add everything to class bitbucket repository

4. [A5 README.md](a5/A5-README.md "My A5 README.md file")

		-Design business card application
		
		-Add everything to class bitbucket repository

4. [P2 README.md](p2/README.md "My P2 README.md file")

		-Added delete and update functions to A5 server validation exercise

		-Created RSS feed

		-added content to site carousel