
# LIS 4381

## Christian Ray

### Assignment 2 Requirements:

1. Create application with two user interfaces
2. Apply buttons and widgets to allow user to access both interfaces
3. Change background color of each interface
4. Document changes and push final project screenshots to bitbucket

![Initial user interface] (img/A2-1.png)



![Secondary user interface] (img/A2-2.png)
