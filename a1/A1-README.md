# LIS 4381

## Christian Ray

### Assignment 1 Requirements:

 -Install Git

-Create repository for assignments

-Define git commands

-Install AMPPS, JDK, and Android Studio

-Build Hello World.java, run through emulator

-Push to bitbucket repository

Git Commands:

1. git init: create an empty Git repository
2. git status: list files changed and those still needed to add or commit
3. git add: add file contents to the index
4. git commit: commit changes to head
5. git push: send changes to repository
6. git pull: fetch from and integrate with another repository of a local branch
7. git clone: creates a working copy of a local repository

![Screenshot 1](img/1.png)

![Screenshot 2](img/2.png)

![Screenshot 3](img/3.png)

https://bitbucket.org/ctr13e/bitbucketstationlocations

https://bitbucket.org/ctr13e/myteamquotes